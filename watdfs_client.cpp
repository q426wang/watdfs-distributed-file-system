// Starter code for CS 454/654
// You SHOULD change this file


#include "watdfs_client.h"
#include "debug.h"
#include <sys/stat.h>
#include <sys/types.h>
INIT_LOG

#include "rpc.h"
#include "watdfs_client_utils.h"
#include <utime.h>
#include <unistd.h>
#include <time.h>
#include <map>
#include <vector>
#include <utility>
#include <iterator>
#include <iostream>
#include "rw_lock.h"


struct file_data{
    int mode;
    uint64_t fh_c;
    time_t Tc;
};

typedef  struct global_data{
    char *_path_to_cache;
    time_t cache_interval;
    std::map<std::string,   struct file_data  > open_file;
}global_d;

// helpers
char* get_full_cache_path(const char *short_path, void *userdata){
    int short_path_len = strlen(short_path);
    global_d *gd = (global_d *)userdata;
    int dir_len = strlen(gd->_path_to_cache);
    int full_len = dir_len + short_path_len + 1;

    char *full_path = (char*)malloc(full_len);

    strcpy(full_path,gd->_path_to_cache);
    strcat(full_path, short_path);

    return full_path;
}

void set_tc(void *userdata, std::string cache_path){
    global_d *gd = (global_d *)userdata;
    ((gd->open_file)[cache_path]).Tc = time(0);
    std::cout << "$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$Tc be set in " << gd->open_file[cache_path].Tc << "】"<< std::endl;
}

void add_to_openfile(void * userdata, std::string str_local_path, int mode, uint64_t fh_local){
    global_d *gd = (global_d *)userdata;
    struct file_data file_c;
    file_c.mode = mode;
    file_c.fh_c = fh_local;
    file_c.Tc = time(0);
    (gd->open_file)[str_local_path] = file_c;
    DLOG("success to add file on userdata");
}

void erase_from_openfile(void *userdata,std::string str_local_path){
    global_d *gd = (global_d *)userdata;
    (gd->open_file).erase(str_local_path);
    DLOG("success to remove file from userdata");
}

int get_mode(void *userdata, std::string str_local_file){
    global_d *gd = (global_d *)userdata;
    return ((gd->open_file)[str_local_file]).mode  & O_ACCMODE;
}

uint64_t get_fh(void *userdata, std::string str_local_file){
    global_d *gd = (global_d *)userdata;
    return ((gd->open_file)[str_local_file]).fh_c;
}

int download_file_to_client(void *userdata, const char *path) {
    char *local_path = get_full_cache_path(path,userdata);
    int fxn_ret = 0;
    std::string str_local_path(local_path);
    struct stat *statbuf_server = (struct stat *) malloc(sizeof(struct stat));
    int rpc_getattr = rpc_cli_getattr((void *)userdata, path, statbuf_server);

    if (rpc_getattr < 0) {
        fxn_ret = rpc_getattr;
        DLOG("fail to impl rpc getattr call in download file to client");
        free(statbuf_server);
        free(local_path);
        return fxn_ret;
    }

    int fh_local = open(local_path, O_RDWR);
    if (fh_local < 0) {
        DLOG("file is not exist in client, create a new file ");
        fxn_ret = mknod(local_path, statbuf_server -> st_mode, statbuf_server -> st_dev);
        fh_local = open(local_path, O_RDWR);
    }
    DLOG("open success in download client sys call");

//    add_to_openfile((void *) userdata,str_local_path,O_RDWR,fh_local);

    int truncate_client = truncate(local_path, statbuf_server->st_size);
    if (truncate_client < 0) {
        DLOG("Returning code in download file to client when system call truncate: %d", truncate_client);
        fxn_ret = -errno;
        free(statbuf_server);
        free(local_path);
        return fxn_ret;
    }

    struct fuse_file_info fi_server;
    fi_server.flags = O_RDONLY;
    int rpc_open = rpc_cli_open((void *)userdata, path, &fi_server);
    if(rpc_open < 0){
        DLOG("Returning code in download file to client when rpc call open: %d", rpc_open);
        fxn_ret = rpc_open;
        free(statbuf_server);
        free(local_path);
        return fxn_ret;
    }

    int rpc_lock = lock(path,RW_READ_LOCK);
    if (rpc_lock < 0){
        DLOG("fail to add read lock in download");
        fxn_ret = rpc_lock;
        free(statbuf_server);
        free(local_path);
        return fxn_ret;
    }
    DLOG("success to add read lock in download");

    char *buf_read_from_server = (char *) malloc(statbuf_server->st_size * sizeof(char));
    int rpc_read = rpc_cli_read((void *)userdata, path, buf_read_from_server, statbuf_server->st_size, 0, &fi_server);
    if (rpc_read < 0) {
        DLOG("Returning code in download file to client when rpc call read: %d", rpc_read);
        fxn_ret = rpc_read;
        free(statbuf_server);
        free(buf_read_from_server);
        free(local_path);
        unlock(path, RW_READ_LOCK);
        return fxn_ret;
    }
    DLOG(" download file to client when rpc call read successful");

    int rpc_unlock = unlock(path, RW_READ_LOCK);
    if(rpc_unlock < 0){
        DLOG("fail to unlock read lock in download");
        fxn_ret = rpc_unlock;
        free(statbuf_server);
        free(buf_read_from_server);
        free(local_path);
        return fxn_ret;
    }

    int write_client = pwrite(fh_local, buf_read_from_server, statbuf_server->st_size, 0);
    DLOG(" download file to client when system call write begin");
    if (write_client < 0) {
        DLOG("Returning code in download file to client when system call write: %d", write_client);
        fxn_ret = -errno;
        free(statbuf_server);
        free(buf_read_from_server);
        free(local_path);
        return fxn_ret;
    }
    DLOG(" download file to client when system call write successful");

    DLOG(" download file to client begin to updata time");
    struct timespec *ts = new struct timespec[2];
    ts[0] = statbuf_server->st_mtim;
    ts[1] = statbuf_server->st_mtim;

    DLOG(" download file to client begin to system call utimensat");
    int utimen_client = utimensat(0, local_path, ts, 0);
    if (utimen_client < 0) {
        DLOG("Returning code in download file to client when system call utimensat: %d", utimen_client);
        fxn_ret = -errno;
        free(statbuf_server);
        free(buf_read_from_server);
        free(local_path);
        return fxn_ret;
    }
    DLOG(" download file to client system call utimensat successful");
//    set_tc((void *)userdata,str_local_path);

    DLOG("download file to client, begin to rpc release");
    int release_server = rpc_cli_release((void *)userdata,path, &fi_server);
    if(release_server < 0){
        DLOG("fail to impl rpc release call in download");
//        memset(statbuf, 0, sizeof(struct stat));
        fxn_ret = release_server;
        free(statbuf_server);
        free(buf_read_from_server);
        free(local_path);
        return fxn_ret;
    }
    DLOG("download file to client, rpc release successful");

    DLOG("download file to client, begin release system call in download");
    int release_client = close(fh_local);
    if(release_client < 0){
        DLOG("fail to release file in client side when download");
        fxn_ret = -errno;
        free(statbuf_server);
        free(buf_read_from_server);
        free(local_path);
        return fxn_ret;
    }
    DLOG("success to release file in client side when download");
//    erase_from_openfile(userdata,str_local_path);

    DLOG(" download file to client begin to free memory");
    free(statbuf_server);
    free(buf_read_from_server);
    free(local_path);
    return fxn_ret;
}

int upload_file_to_server(void *userdata, const char *path){
    char *local_path = get_full_cache_path(path,userdata);
    int fxn_ret = 0;
    std::string str_local_path(local_path);

    DLOG("upload file to server,  begin open system call ################################");
//    int fh_local = open(local_path, O_RDONLY | O_CREAT, 00777);
    int fh_local = open(local_path, O_RDONLY );
    if(fh_local<0){
        DLOG("upload file to server,  fail to impl open system call");
        fxn_ret = -errno;
        free(local_path);
        return fxn_ret;
    }
//    add_to_openfile((void *) userdata,str_local_path,O_RDWR,fh_local);
    DLOG("upload file to server, open system call successful");

    DLOG("upload file to server , begin to system call stat");
    struct stat *statbuf_client = (struct stat *) malloc(sizeof(struct stat));
    int stat_client = stat(local_path, statbuf_client);
    if(stat_client < 0){
        DLOG("Returning code in upload file to server when system call stat: %d", statbuf_client);
        fxn_ret = -errno;
        free(statbuf_client);
        free(local_path);
        return fxn_ret;
    }
    DLOG("upload file to server, stat system call successful");

    struct fuse_file_info fi_server;
    fi_server.flags = O_WRONLY;
    int open_server = rpc_cli_open(userdata,path,&fi_server);
    if(open_server < 0){
        DLOG("Returning code in upload file to server when rpc open: %d", open_server);
        fxn_ret = open_server;
        free(local_path);
        return fxn_ret;
    }
    DLOG("upload file to server, rpc open successful");

    int rpc_lock = lock(path, RW_WRITE_LOCK);
    if(rpc_lock < 0){
        DLOG("fail to add write lock in upload");
        fxn_ret = rpc_lock;
        free(local_path);
        return fxn_ret;
    }
    DLOG("success to add write lock in upload");

    int truncate_server = rpc_cli_truncate((void *)userdata, path, statbuf_client->st_size);
    if(truncate_server < 0){
        DLOG("Returning code in upload file to server when rpc call truncate: %d", truncate_server);
        fxn_ret = truncate_server;
        free(statbuf_client);
        free(local_path);
        unlock(path, RW_WRITE_LOCK);
        return fxn_ret;
    }
    DLOG("upload file to server,  rpc cli truncate successful");

    DLOG("upload file to server,  begin to pread system call");
    char * buf_read_from_client =  (char *) malloc(statbuf_client->st_size * sizeof(char));
    int read_client = pread(fh_local, buf_read_from_client, statbuf_client->st_size, 0);
    if(read_client < 0){
        DLOG("Returning code in upload file to server when system call pread: %d", read_client);
        fxn_ret = -errno;
        free(statbuf_client);
        free(local_path);
        free(buf_read_from_client);
        unlock(path, RW_WRITE_LOCK);
        return fxn_ret;
    }
    DLOG("upload file to server,  pread system call successful");

    DLOG("upload file to server, begin to rpc cli write");
    int write_server = rpc_cli_write((void *)userdata, path, buf_read_from_client, statbuf_client->st_size,0, &fi_server);
    if(write_server < 0 ){
        DLOG("Returning code in upload file to server when rpc call write: %d", write_server);
        fxn_ret = write_server;
        free(statbuf_client);
        free(local_path);
        free(buf_read_from_client);
        unlock(path, RW_WRITE_LOCK);
        return fxn_ret;
    }
    DLOG("upload file to server,  rpc cli write successful");

    int rpc_unlock = unlock(path, RW_WRITE_LOCK);
    if(rpc_unlock < 0){
        DLOG("fail to unlock write lock in upload");
        fxn_ret = rpc_unlock;
        free(statbuf_client);
        free(local_path);
        free(buf_read_from_client);
        return fxn_ret;
    }
    DLOG("success to unlock write lock in upload");

    struct timespec * ts = new struct timespec [2];
    ts[0] = statbuf_client->st_mtim;
    ts[1] = statbuf_client->st_mtim;

    DLOG("upload file to server,  begin to rpc cli utimens");
    int utimens_server = rpc_cli_utimens((void *)userdata, path, ts);
    if(utimens_server < 0){
        DLOG("Returning code in upload file to server when rpc call utimens: %d", utimens_server);
        fxn_ret = utimens_server;
        free(statbuf_client);
        free(local_path);
        free(buf_read_from_client);
        return fxn_ret;
    }
    DLOG("upload file to server,  rpc cli utimens successful");
//    set_tc((void *)userdata,str_local_path);

    DLOG("upload file to server, begin release system call in upload");
    int release_client = close(fh_local);
    if(release_client < 0){
        DLOG("fail to release file in server side when upload");
        fxn_ret = -errno;
        free(statbuf_client);
        free(local_path);
        free(buf_read_from_client);
        return fxn_ret;
    }
    DLOG("success to release file in client side when upload");
//    erase_from_openfile(userdata,str_local_path);

    DLOG("upload file to client, begin to rpc release");
    int release_server = rpc_cli_release((void *)userdata,path,&fi_server);
    if(release_server < 0){
        DLOG("fail to impl rpc release call in upload");
        fxn_ret = release_server;
        free(statbuf_client);
        free(local_path);
        free(buf_read_from_client);
        return fxn_ret;
    }
    DLOG("upload file to server, rpc release successful");

    free(statbuf_client);
    free(local_path);
    free(buf_read_from_client);
    return fxn_ret;
}

bool check_freshness(void *userdata, const char *path){
//    std::cout << "$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$time(0) " << gd->open_file[cache_path].Tc << "】"<< std::endl;
    int sys_ret, rpc_ret;
    bool res = false;
    global_d *gd = (global_d *)userdata;
    time_t t = gd->cache_interval;
    char *local_path = get_full_cache_path(path,(void *)userdata);
    std::string str_local_path(local_path);
    time_t Tc = (gd->open_file)[str_local_path].Tc;

    if((time(0) - Tc) < t){
//        std::cout << "【T - tc = " << time(0)-((global_d *)userdata)->open_file[str_local_path].Tc << "】"<< std::endl;
//        DLOG("【T - tc < t, fresh】");
        DLOG("It is a fresh copy in cache T-Tc < t");
        res = true;
    }else{
        struct stat *statbuf_client = (struct stat *) malloc(sizeof(struct stat));
        sys_ret = stat(local_path, statbuf_client);
        if(sys_ret<0){
            DLOG("fail to impl system call in freshness check");
        }

        struct stat *statbuf_server = (struct stat *) malloc(sizeof(struct stat));
        rpc_ret = rpc_cli_getattr(userdata, path, statbuf_server);
        if(rpc_ret<0){
            DLOG("fail to impl rpc call in freshness check");
        }

        if(statbuf_client->st_mtime == statbuf_server->st_mtime){
            set_tc((void *)userdata,str_local_path);
            res = true;
            DLOG("It is a fresh copy in cache T-server == T-client and updata Tc");
        }
        free(statbuf_client);
        free(statbuf_server);
    }
    free(local_path);
    return res;
}

bool is_open(void *userdata , const char *path){
    global_d *gd = (global_d *)userdata;
    char *local_path = get_full_cache_path(path,userdata);
    std::string str_local_path(local_path);
    if((gd->open_file).find(str_local_path) != (gd->open_file).end()){
        DLOG("file is opened");
        std::cout << "userdata fh_c::" << (gd->open_file)[str_local_path].fh_c <<std::endl;
        std::cout << "userdata mode::" << (gd->open_file)[str_local_path].mode <<std::endl;

        free(local_path);
        return true;
    }
    free(local_path);
    DLOG("file is not open, close");
    return false;
}


// SETUP AND TEARDOWN
void *watdfs_cli_init(struct fuse_conn_info *conn, const char *path_to_cache,
                      time_t cache_interval, int *ret_code) {
    // TODO: set up the RPC library by calling `rpcClientInit`.
    int rpcCInit = rpcClientInit();
    // TODO: check the return code of the `rpcClientInit` call
    // `rpcClientInit` may fail, for example, if an incorrect port was exported.
//    # ifdef PRINT_ERR
    if (rpcCInit < 0) {
        DLOG("Failed to initialize RPC Client");
//        std::cerr << "Failed to initialize RPC Client" << std::endl;
    } else {
        DLOG("success to initialize RPC Client");
//        std::cout << "success to initialize RPC Client"
    }
//     #endif
    // It may be useful to print to stderr or stdout during debugging.
    // Important: Make sure you turn off logging prior to submission!
    // One useful technique is to use pre-processor flags like:
    // # ifdef PRINT_ERR
    // std::cerr << "Failed to initialize RPC Client" << std::endl;
    // #endif
    // Tip: Try using a macro for the above to minimize the debugging code.

    // TODO Initialize any global state that you require for the assignment and return it.
    // The value that you return here will be passed as userdata in other functions.
    // In A1, you might not need it, so you can return `nullptr`.
    global_d *gd = new struct global_data;
    void *userdata = (void *)gd;

    // TODO: save `path_to_cache` and `cache_interval` (for A3).
    gd->_path_to_cache = (char *)malloc(strlen(path_to_cache)+1);
    strcpy(gd->_path_to_cache, path_to_cache);
    gd->cache_interval = cache_interval;
    // TODO: set `ret_code` to 0 if everything above succeeded else some appropriate
    // non-zero value.
    *ret_code = rpcCInit;

    // Return pointer to global state data.
    return (void *) userdata;
}

void watdfs_cli_destroy(void *userdata) {
    // TODO: clean up your userdata state.
//    userdata = (struct global_data *) userdata;
//
//    std::map<std::string, struct file_data *>:: iterator itr;
//    for (itr = ((struct user_data *)userdata)->openfiles.begin(); itr != ((struct user_data *)userdata)->openfiles.end(); itr++){
//        delete itr->second;
//    }
    global_d *gd = (global_d *)userdata;
    free(gd->_path_to_cache);
    userdata = nullptr;
//    delete userdata;

    // TODO: tear down the RPC library by calling `rpcClientDestroy`.
    rpcClientDestroy();
}

// GET FILE ATTRIBUTES
int watdfs_cli_getattr(void *userdata, const char *path, struct stat *statbuf) {
    DLOG("receive client getattr call");

    int fxn_ret = 0;
    int retcode, getattr_client, close_client;
    char *local_path = get_full_cache_path(path,userdata);
    std::string str_local_path(local_path);

    if(! is_open(userdata,path)){
        retcode = download_file_to_client(userdata,path);
        if(retcode < 0){
            fxn_ret = retcode;
            DLOG("fail to impl system call stat in client getattr");
            memset(statbuf, 0, sizeof(struct stat));
            free(local_path);
            return fxn_ret;
        }else{
            retcode = open(local_path,O_RDONLY);
//            add_to_openfile(userdata,str_local_path,O_RDONLY,retcode);
//            set_tc(userdata, str_local_path);
            getattr_client = stat(local_path,statbuf );
            if(getattr_client < 0 ){
                DLOG("fail to impl system call stat in client getattr");
                memset(statbuf, 0, sizeof(struct stat));
                fxn_ret = -errno;
                free(local_path);
                return fxn_ret;
            }
            DLOG("success to impl system call stat in client getattr");
            close_client = close(retcode);
//            erase_from_openfile(userdata,str_local_path);
        }
    } else{
        int mode = get_mode(userdata,str_local_path);
        if(mode == O_RDONLY){
            if(!check_freshness(userdata,path)){
                retcode = download_file_to_client(userdata,path);
                set_tc(userdata,str_local_path);
                if(retcode < 0) {
                    fxn_ret = retcode;
                    memset(statbuf, 0, sizeof(struct stat));
                    free(local_path);
                    return fxn_ret;
                }
            }
        }
        getattr_client = stat(local_path,statbuf );
        if(getattr_client < 0 ){
            DLOG("fail to impl system call stat in client getattr");
            memset(statbuf, 0, sizeof(struct stat));
            fxn_ret = -errno;
            free(local_path);
            return fxn_ret;
        }
        DLOG("success to impl system call stat in client getattr");
    }
    free(local_path);
    return fxn_ret;
}

// rpc for client_getattr
int rpc_cli_getattr(void *userdata, const char *path, struct stat *statbuf){
    // SET UP THE RPC CALL
//    DLOG("Received getattr call...");

    // getattr has 3 arguments.
    int ARG_COUNT = 3;

    // Allocate space for the output arguments.
    void **args = (void **) malloc(ARG_COUNT * sizeof(void *));

    // Allocate the space for arg types, and one extra space for the null
    // array element.
    int arg_types[ARG_COUNT + 1];

    // The path has string length (strlen) + 1 (for the null character).
    int pathlen = strlen(path) + 1;

    // Fill in the arguments
    // The first argument is the path, it is an input only argument, and a char
    // array. The length of the array is the length of the path.
    arg_types[0] =
            (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint) pathlen;
    // For arrays the argument is the array pointer, not a pointer to a pointer.
    args[0] = (void *) path;

    arg_types[1] = (1u << ARG_OUTPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) |
                   (uint)sizeof(struct stat); // statbuf
    args[1] = (void *) statbuf;

    arg_types[2] = (1u << ARG_OUTPUT) | (ARG_INT << 16u);

    int retcode_getattr;
    args[2] = (void *) &retcode_getattr;

    // Finally, the last position of the arg types is 0. There is no
    // corresponding arg.
    arg_types[3] = 0;

    // MAKE THE RPC CALL
    int rpc_ret = rpcCall((char *) "getattr", arg_types, args);

    // HANDLE THE RETURN
    // The integer value watdfs_cli_getattr will return.
    int fxn_ret = 0;
    if (rpc_ret < 0) {
//        DLOG("rpc call failed in client getattr");
        fxn_ret = -EINVAL;
    } else {
//        DLOG("rpc call success in client getattr");
        fxn_ret = retcode_getattr;
    }

    if (fxn_ret < 0) {
//        DLOG(" failed to get client getattr");
        memset(statbuf, 0, sizeof(struct stat));
    }

    // Clean up the memory we have allocated.
    free(args);

    // Finally return the value we got from the server.
//    DLOG("getattr rpc call retcode %d\n", fxn_ret);
    return fxn_ret;
}

// client fgetattr
int watdfs_cli_fgetattr(void *userdata, const char *path, struct stat *statbuf,
                        struct fuse_file_info *fi) {
    DLOG("receive client fgetattr call");

    char *local_path = get_full_cache_path(path,userdata);
    std::string str_local_path(local_path);
    int fxn_ret = 0;
    int retcode, download_client,close_client, fgetattr_client;

    if(!is_open(userdata,path)){
        download_client =  download_file_to_client(userdata,path);
        if(download_client < 0){
            fxn_ret = download_client;
            DLOG("fail to impl system call stat in client getattr");
            memset(statbuf, 0, sizeof(struct stat));
            free(local_path);
            return fxn_ret;
        } else{
            retcode = open(local_path,fi->flags);
//            add_to_openfile(userdata,str_local_path,fi->flags,retcode);
//            set_tc(userdata,str_local_path);
            fgetattr_client = fstat(retcode ,statbuf );
            if(fgetattr_client < 0 ){
                DLOG("fail to impl system call stat in client getattr");
                memset(statbuf, 0, sizeof(struct stat));
                fxn_ret = -errno;
                free(local_path);
                return fxn_ret;
            }
            DLOG("success to impl system call stat in client getattr");
            close_client = close(retcode);
//            erase_from_openfile(userdata,str_local_path);
        }
    } else{
        int mode = get_mode(userdata,str_local_path);
        if(mode == O_RDONLY){
            if(!check_freshness(userdata,path)){
                retcode = download_file_to_client(userdata,path);
                set_tc(userdata,str_local_path);
                if(retcode < 0) {
                    fxn_ret = retcode;
                    memset(statbuf, 0, sizeof(struct stat));
                    free(local_path);
                    return fxn_ret;
                }
            }
        }
        int fh_local = get_fh(userdata,str_local_path);
        fgetattr_client = fstat(fh_local,statbuf );
        if(fgetattr_client < 0){
            DLOG("fail to impl fstat sys call in client fgetattr");
            fxn_ret = -errno;
            memset(statbuf, 0, sizeof(struct stat));
            free(local_path);
            return fxn_ret;
        }
        DLOG("success to impl fstat sys call in client fgetattr");
    }
    free(local_path);
    return fxn_ret;
}

// rpc for client fgetattr
int rpc_cli_fgetattr(void *userdata, const char *path, struct stat *statbuf,
                 struct fuse_file_info *fi){
    // SET UP THE RPC CALL
//    DLOG("Received fgetattr call...");

    // fgetattr has 4 arguments.
    int ARG_COUNT = 4;

    // Allocate space for the output arguments.
    void **args = (void **) malloc(ARG_COUNT * sizeof(void *));

    int arg_types[ARG_COUNT + 1];

    int pathlen = strlen(path) + 1;

    arg_types[0] =
            (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint) pathlen;
    args[0] = (void *) path;

    arg_types[1] = (1u << ARG_OUTPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) |
                   (uint)
    sizeof(struct stat); // statbuf
    args[1] = (void *) statbuf;

    arg_types[2] =
            (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) |
            (uint)
    sizeof(struct fuse_file_info);// fuse_file_info
    args[2] = (void *) fi;

    arg_types[3] = (1u << ARG_OUTPUT) | (ARG_INT << 16u);
    int retcode_fgetattr;
    args[3] = (void *) &retcode_fgetattr;

    arg_types[4] = 0;

//    DLOG("start rpccall for fgetattr");
    int rpc_ret = rpcCall((char *) "fgetattr", arg_types, args);

    // HANDLE THE RETURN
    // The integer value watdfs_cli_getattr will return.
    int fxn_ret = 0;
    if (rpc_ret < 0) {
//        DLOG("fail in rpc call for fgetattr");
        fxn_ret = -EINVAL;
    } else {
//        DLOG("success to Received mknod call in RPC Client");
        fxn_ret = retcode_fgetattr;
    }

    if (fxn_ret < 0) {
//        DLOG(" failed to get system call for fgetattr in server");
        memset(statbuf, 0, sizeof(struct stat));
    }

    free(args);

    return fxn_ret;
}

// CREATE, OPEN AND CLOSE
int watdfs_cli_mknod(void *userdata, const char *path, mode_t mode, dev_t dev) {

    int fxn_ret = 0;
    char * local_path = get_full_cache_path(path,userdata);
    std::string str_local_path(local_path);
    int mknod_client, mknod_server;

    if(!is_open(userdata,path)){
        mknod_client = mknod(local_path, mode, dev);
        if(mknod_client < 0){
            DLOG("fail to impl mknod system call in cli_mknod");
            fxn_ret = -errno;
            free(local_path);
            return fxn_ret;
        }
        DLOG("success to impl mknod sys call in cli_mknod");
        fxn_ret = rpc_cli_mknod(userdata,path,mode,dev);
        if(fxn_ret < 0){
            DLOG("fail to rpc mknod in client when not open cli_mknod");
            free(local_path);
            return fxn_ret;
        }

        fxn_ret = upload_file_to_server(userdata,path);
        if(fxn_ret < 0){
            DLOG("fail to upload in client when not open cli_mknod");
            free(local_path);
            return fxn_ret;
        }
    } else{
        int mode = get_mode(userdata,str_local_path);
        if(mode == O_RDONLY){
            free(local_path);
            return -EMFILE;
        } else{
            mknod_client = mknod(local_path, mode, dev);
            if(mknod_client < 0){
                DLOG("fail to impl mknod system call in cli_mknod");
                fxn_ret = -errno;
                free(local_path);
                return fxn_ret;
            }
            DLOG("success to impl mknod sys call in cli_mknod");
            if(!check_freshness(userdata,path)){
                fxn_ret = upload_file_to_server(userdata,path);
                set_tc(userdata,str_local_path);
                if(fxn_ret < 0){
                    DLOG("fail to upload in client when not open cli_mknod");
                    free(local_path);
                    return fxn_ret;
                }
            }
        }
    }
    free(local_path);
    return fxn_ret;
}

// rpc for clent mknod
int rpc_cli_mknod(void *userdata, const char *path, mode_t mode, dev_t dev){
    // Called to create a file. SET UP THE RPC CALL
//    DLOG("Received mknod call...");

    // fgetattr has 4 arguments.
    int ARG_COUNT = 4;

    // Allocate space for the output arguments.
    void **args = (void **) malloc(ARG_COUNT * sizeof(void *));

    int arg_types[ARG_COUNT + 1];

    int pathlen = strlen(path) + 1;

    arg_types[0] =
            (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint) pathlen;
    args[0] = (void *) path;

    arg_types[1] = (1u << ARG_INPUT) | (ARG_INT << 16u);
    args[1] = (void *) &mode;

    arg_types[2] =
            (1u << ARG_INPUT) | (ARG_LONG << 16u) | (uint) dev;
    args[2] = (void *) &dev;

    arg_types[3] = (1u << ARG_OUTPUT) | (ARG_INT << 16u);
    int retcode_mknod;
    args[3] = (void *) &retcode_mknod;
//    DLOG("mknod rpc call retcode from server %d\n", retcode_mknod);

    arg_types[4] = 0;
//    DLOG("start mknod call in RPC Client.....");
    int rpc_ret = rpcCall((char *) "mknod", arg_types, args);

    int fxn_ret = 0;
    if (rpc_ret < 0) {
//        DLOG("fail to Received mknod call in RPC Client");
        fxn_ret = -EINVAL;
    } else {
//        DLOG("success to Received mknod call in RPC Client");
        fxn_ret = retcode_mknod;
    }

    if (fxn_ret < 0) {
//        DLOG("Failed to impl system mknod call in RPC server");
    }
    free(args);
//    DLOG("mknod rpc call retcode %d\n", fxn_ret);
    return fxn_ret;
}

int watdfs_cli_open(void *userdata, const char *path, struct fuse_file_info *fi) {
    DLOG("receive cli_open in client side");

    int fxn_ret = 0;
    int fh_local,    getattr_client;
    char *local_path = get_full_cache_path( path ,userdata);
    std::string str_local_path(local_path);

    if(is_open(userdata,path)){
        DLOG("file is been opened multitimes in client");
        free(local_path);
        return -EMFILE;
    } else{
        struct stat stat_server;
        getattr_client = rpc_cli_getattr(userdata,path,&stat_server);
        if(getattr_client < 0){
            if(fi->flags != O_CREAT ){
                free(local_path);
                return getattr_client;
            }
        }
        fxn_ret = rpc_cli_open(userdata,path,fi);
        fxn_ret = download_file_to_client(userdata,path);
        fh_local = open(local_path,fi->flags);
        add_to_openfile(userdata,str_local_path,fi->flags,fh_local);
        set_tc(userdata,str_local_path);
    }
    free(local_path);
    return fxn_ret;
}

// rpc for client open
int rpc_cli_open(void *userdata, const char *path, struct fuse_file_info *fi){
    // Called during open.
//    DLOG("Received open call...");

    // open has 3 arguments.
    int ARG_COUNT = 3;

    // Allocate space for the output arguments.
    void **args = (void **) malloc(ARG_COUNT * sizeof(void *));

    int arg_types[ARG_COUNT + 1];

    int pathlen = strlen(path) + 1;

    arg_types[0] =
            (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint) pathlen;
    args[0] = (void *) path;

    arg_types[1] = (1u << ARG_INPUT) | (1u << ARG_OUTPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint)
    sizeof(struct fuse_file_info);
    args[1] = (void *) fi;

    arg_types[2] = (1u << ARG_OUTPUT) | (ARG_INT << 16u);
    int retcode_open;
    args[2] = (void *) &retcode_open;

    arg_types[3] = 0;

    // You should fill in fi->fh.

//    DLOG("Start rpccall in open server....");
    int rpc_ret = rpcCall((char *) "open", arg_types, args);
//    fi->fh = rpc_ret;

    // HANDLE THE RETURN
    // The integer value watdfs_cli_open will return.
    int fxn_ret = 0;
    if (rpc_ret < 0) {
//        DLOG("fail to impl rpccall in open client");
        fxn_ret = -EINVAL;
    } else {
//        DLOG("success to impl rpc call in release client");
        fxn_ret = retcode_open;
    }

    if (fxn_ret < 0) {
//        DLOG("Failed to impl system open call in RPC server");
    }

    free(args);
    return fxn_ret;
}

int watdfs_cli_release(void *userdata, const char *path,
                       struct fuse_file_info *fi) {
    DLOG("receive release call in cli_release");
    char * local_path = get_full_cache_path(path,userdata);
    std::string str_local_path(local_path);
    int fxn_ret = 0;

    int mode = get_mode(userdata,str_local_path);
    if(mode != O_RDONLY){
        DLOG("begin to upload the client file to server");
        int upload_server = upload_file_to_server(userdata,path);
        set_tc(userdata,str_local_path);
        if(upload_server < 0){
            DLOG("fail to upload the client file to server when cli_release");
            fxn_ret = upload_server;
            free(local_path);
            return fxn_ret;
        }
        DLOG("success to upload the client file to server when cli_release");
    }
    uint64_t fh_local = get_fh(userdata,str_local_path);

    DLOG("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
    DLOG("fh in cli_release read  in userdata: %d",fh_local);
    DLOG("^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
    if(fh_local < 0){
        DLOG("fail to get fh in client cache file");
        free(local_path);
        return fh_local;
    } else{
        DLOG("success to receive fh cache in client side");
    }

    DLOG("set openfile info fi into neg");
    erase_from_openfile(userdata,str_local_path);

    DLOG("begin to impl close sys call in cli_release");
    int release_client = close(fh_local);
    if(release_client < 0){
        DLOG("fail to impl close sys call in cli_release");
        fxn_ret = -errno;
        free(local_path);
        return fxn_ret;
    }
    DLOG("success to impl close sys call in cli_release");

    DLOG("begin to impl rpc release in cli_release");
    int rpc_release = rpc_cli_release(userdata, path, fi);
    if(rpc_release < 0){
        DLOG("fail to impl rpc release in cli_release");
        fxn_ret = rpc_release;
        free(local_path);
        return fxn_ret;
    }
    DLOG("success impl rpc_release in cli_release");
    free(local_path);
    return fxn_ret;
}

// rpc for client release
int rpc_cli_release(void *userdata, const char *path, struct fuse_file_info *fi){
    // Called during close, but possibly asynchronously.
//    DLOG("Received release call...");

    // open has 3 arguments.
    int ARG_COUNT = 3;

    // Allocate space for the output arguments.
    void **args = (void **) malloc(ARG_COUNT * sizeof(void *));

    int arg_types[ARG_COUNT + 1];

    int pathlen = strlen(path) + 1;

    arg_types[0] =
            (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint) pathlen;
    args[0] = (void *) path;

    arg_types[1] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint)
    sizeof(struct fuse_file_info);
    args[1] = (void *) fi;

    arg_types[2] = (1u << ARG_OUTPUT) | (ARG_INT << 16u);
    int retcode_release;
    args[2] = (void *) &retcode_release;

    arg_types[3] = 0;

//    DLOG("Start rpc call in release client");
    int rpc_ret = rpcCall((char *) "release", arg_types, args);

    int fxn_ret = 0;
    if (rpc_ret < 0) {
//        DLOG("fail to impl rpc call in release client");
        fxn_ret = -EINVAL;
    } else {
//        DLOG("success to impl rpc call in release client");
        fxn_ret = retcode_release;
    }

    if (fxn_ret < 0) {
//        DLOG("Failed to impl system release call in RPC server");
    }

    free(args);
    return fxn_ret;
}

// READ AND WRITE DATA
int watdfs_cli_read(void *userdata, const char *path, char *buf, size_t size,
                    off_t offset, struct fuse_file_info *fi) {
    DLOG("receive client read call cache");
    int fxn_ret = 0;
    if(! is_open(userdata,path)){
        DLOG("can't read before open");
        fxn_ret = -EPERM;
        return fxn_ret;
    }

    char *local_path = get_full_cache_path(path,userdata);
    std::string str_local_path(local_path);

    int mode = get_mode(userdata,str_local_path);
    if(mode == O_RDONLY){
        if(!check_freshness(userdata,path)){
            int r_read = download_file_to_client(userdata,path);
            set_tc(userdata,str_local_path);
            DLOG("success to download newest file in cli read");
        }
    }
    uint64_t fh_local = get_fh(userdata,str_local_path);

    DLOG("begin to system call pread in cli_read");
    int read_client = pread(fh_local, buf, size, offset);
    if(read_client < 0){
        DLOG("fail to sys call pread in cli_read");
        fxn_ret = -errno;
        free(local_path);
        return fxn_ret;
    }

    DLOG("success to sys call pread in cli_read");
    fxn_ret = read_client;
    free(local_path);
    return fxn_ret;
}

// rpc for client read
int rpc_cli_read(void *userdata, const char *path, char *buf, size_t size,
             off_t offset, struct fuse_file_info *fi){
//    DLOG("Received read call...");

    int ARG_COUNT = 6;

    // Allocate space for the output arguments.
    void **args = (void **) malloc(ARG_COUNT * sizeof(void *));

    int arg_types[ARG_COUNT + 1];

    int pathlen = strlen(path) + 1;

    int retcode_read;
    arg_types[0] =
            (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint) pathlen;
    arg_types[1] = (1u << ARG_OUTPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint)size;
    arg_types[2] = (1u << ARG_INPUT) | (ARG_LONG << 16u);
    arg_types[3] = (1u << ARG_INPUT) | (ARG_LONG << 16u);
    arg_types[4] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint)  sizeof(struct fuse_file_info);
    arg_types[5] = (1u << ARG_OUTPUT) | (ARG_INT << 16u);
    arg_types[6] = 0;
    args[0] = (void *)path;
    args[1] = (void *) buf;
    args[2] = (void *) &size;
    args[3] = (void *) &offset;
    args[4] = (void *) fi;
    args[5] = (void *) &retcode_read;

    int rpc_ret ;
    int total = 0;
    int fxn_ret;

//    DLOG("start check if size <= MAX_ARRAY_LEN");
    if(size< MAX_ARRAY_LEN){
//        DLOG(" size <= MAX_ARRAY_LEN.....");

//        DLOG("start rpccall in read client");
        rpc_ret = rpcCall((char *) "read", arg_types, args);
        if(rpc_ret< 0){
            fxn_ret = -EINVAL;
        }else{
            fxn_ret = retcode_read;
        }
    }
    else{
        size_t remain_size = size;
        size = MAX_ARRAY_LEN;
//        DLOG(" size >= MAX_ARRAY_LEN...........................");
        arg_types[1] = (1u << ARG_OUTPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint)size;

        args[2] = (void *) &size;

        do{
//            DLOG("retcode_read %d\n", retcode_read);
//            DLOG("args[5] %d\n", *(int *) args[5]);
//            DLOG("total %d\n", total);
//            DLOG("offset %d\n", offset);
//            DLOG("buf %d\n", buf);
            if(remain_size < MAX_ARRAY_LEN){
                arg_types[1] = (1u << ARG_OUTPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint)remain_size;
                args[2] = (void *) &remain_size;
//                DLOG("start rpccall in read client when size >= MAX_ARRAY_LEN and now size < MAX_ARRAY_LEN");
                rpc_ret = rpcCall((char *) "read", arg_types, args);
                if(rpc_ret< 0){
                    fxn_ret = -EINVAL;
                    total = total+retcode_read;

                    buf = buf+retcode_read;
                    args[1] = (void *)buf;

                    offset = offset + retcode_read;
                    args[3] = (void *) &offset;

                }else{
                    total = total+retcode_read;

                    buf = buf+retcode_read;
                    args[1] = (void *)buf;

                    offset = offset + retcode_read;
                    args[3] = (void *) &offset;

                    fxn_ret = total;
                }

                break;
            }
            rpc_ret = rpcCall((char *) "read", arg_types, args);
            if(rpc_ret < 0){
                fxn_ret = -EINVAL;
                total = total+retcode_read;

                buf = buf+retcode_read;
                args[1] = (void *)buf;

                offset = offset + retcode_read;
                args[3] = (void *) &offset;

                remain_size = remain_size - retcode_read;
                break;
            } else{
                total = total+retcode_read;

                buf = buf+retcode_read;
                args[1] = (void *)buf;

                offset = offset + retcode_read;
                args[3] = (void *) &offset;

                remain_size = remain_size - retcode_read;
            }
        }while (retcode_read>0);

        if(rpc_ret< 0){
            fxn_ret = -EINVAL;
        }else{
            fxn_ret = total;
        }
    }

    free(args);
//    DLOG("read rpc call retcode %d\n", fxn_ret);
    return fxn_ret;
}

int watdfs_cli_write(void *userdata, const char *path, const char *buf,
                     size_t size, off_t offset, struct fuse_file_info *fi) {
    DLOG("receive cli write in cli_write cache");
    if(!is_open(userdata,path)){
        return -EPERM;
    }

    char *local_path = get_full_cache_path(path,userdata);
    std::string str_local_path(local_path);
    int fxn_ret = 0;

    int mode = get_mode(userdata,str_local_path);

    if(mode == O_RDONLY) {
        free(local_path);
        return -EMFILE;
    }

    DLOG("begin to find cache local file fh local in cli_write");
    uint64_t fh_local = get_fh(userdata,str_local_path);

    DLOG("begin to impl pwrite in cli_write");
    int write_client = pwrite(fh_local,buf,size,offset);
    if(write_client < 0){
        DLOG("fail to impl pwrite in cli_write");
        fxn_ret = -errno;
        free(local_path);
        return fxn_ret;
    }
    DLOG("success to impl sys call pwrite in cli_write");

    DLOG("begin to check if local cache file is fresh or not in cli_write");
    if(!check_freshness((void *)userdata,path)){
        DLOG("local cache file writes something new in cli_write and upload the new file");
        int upload_server = upload_file_to_server((void *)userdata,path);
        set_tc((void *)userdata,str_local_path);
        if(upload_server < 0){
            DLOG("upload cache file fail to srever in cli_write");
            fxn_ret = upload_server;
            free(local_path);
            return fxn_ret;
        }
    }
    fxn_ret = write_client;
    free(local_path);
    return fxn_ret;
}

//rpc for client write
int rpc_cli_write(void *userdata, const char *path, const char *buf,
              size_t size, off_t offset, struct fuse_file_info *fi){
    // Write size amount of data at offset of file from buf.

    // Remember that size may be greater then the maximum array size of the RPC
    // library.
//    DLOG("Received write call...");

    int ARG_COUNT = 6;

    // Allocate space for the output arguments.
    void **args = (void **) malloc(ARG_COUNT * sizeof(void *));

    int arg_types[ARG_COUNT + 1];

    int pathlen = strlen(path) + 1;
    int retcode_write;
    arg_types[0] =
            (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint) pathlen;
    arg_types[1] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint)size;
    arg_types[2] = (1u << ARG_INPUT) | (ARG_LONG << 16u);
    arg_types[3] = (1u << ARG_INPUT) | (ARG_LONG << 16u);
    arg_types[4] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint)  sizeof(struct fuse_file_info);
    arg_types[5] = (1u << ARG_OUTPUT) | (ARG_INT << 16u);
    arg_types[6] = 0;
    args[0] = (void *)path;
    args[1] = (void *) buf;
    args[2] = (void *) &size;
    args[3] = (void *) &offset;
    args[4] = (void *) fi;
    args[5] = (void *) &retcode_write;

    int rpc_ret ;
    int total = 0;
    int fxn_ret;

//    DLOG("start check if size <= MAX_ARRAY_LEN");
    if(size< MAX_ARRAY_LEN){
//        DLOG(" size <= MAX_ARRAY_LEN.....");

//        DLOG("start rpccall in write client");
        rpc_ret = rpcCall((char *) "write", arg_types, args);
        if(rpc_ret< 0){
            fxn_ret = -EINVAL;
        }else{
            fxn_ret = retcode_write;
        }
    }
    else{
        size_t remain_size = size;
        size = MAX_ARRAY_LEN;
//        DLOG(" size > MAX_ARRAY_LEN...........................................");
        arg_types[1] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint)size;

        args[2] = (void *) &size;

        do{
            if(remain_size < MAX_ARRAY_LEN){
                arg_types[1] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint)remain_size;
                args[2] = (void *) &remain_size;
//                DLOG("start rpccall in write client");
                rpc_ret = rpcCall((char *) "write", arg_types, args);

                if(rpc_ret< 0){
                    fxn_ret = -EINVAL;
                    total = total+retcode_write;

                    buf = buf+retcode_write;
                    args[1] = (void *)buf;

                    offset = offset + retcode_write;
                    args[3] = (void *) &offset;

                }else{
                    total = total+retcode_write;

                    buf = buf+retcode_write;
                    args[1] = (void *)buf;

                    offset = offset + retcode_write;
                    args[3] = (void *) &offset;

                    fxn_ret = total;
                }
                break;
            }
            rpc_ret = rpcCall((char *) "write", arg_types, args);
            if(rpc_ret < 0){
                fxn_ret = -EINVAL;
                total = total+retcode_write;

                buf = buf+retcode_write;
                args[1] = (void *)buf;

                offset = offset + retcode_write;
                args[3] = (void *) &offset;

                remain_size = remain_size - retcode_write;
                break;
            } else{
                total = total+retcode_write;

                buf = buf+retcode_write;
                args[1] = (void *)buf;

                offset = offset + retcode_write;
                args[3] = (void *) &offset;

                remain_size = remain_size - retcode_write;
            }
        }while (retcode_write>0);
        if(rpc_ret <0){
//            DLOG("fail to impl rpccall in write client");
            fxn_ret = -EINVAL;
        } else {
//            DLOG("success to impl rpc call in write client");
            fxn_ret = total;
        }
    }

    free(args);
//    DLOG("write rpc call retcode %d\n", fxn_ret);

    return fxn_ret;
}

int watdfs_cli_truncate(void *userdata, const char *path, off_t newsize) {
    DLOG("receive cli truncate in cli_truncate cache");

    char *local_path = get_full_cache_path(path, userdata);
    std::string str_local_path(local_path);
    int fxn_ret = 0;
    int retcode,truncate_client,close_client;

    if(!is_open(userdata,path)){
        retcode = download_file_to_client(userdata,path);
        if(retcode < 0){
            fxn_ret = retcode;
            DLOG("fail to impl system call stat in client getattr");
            free(local_path);
            return fxn_ret;
        }else{
            retcode = open(local_path,O_RDWR);
//            add_to_openfile(userdata,str_local_path, O_RDWR ,retcode);
//            set_tc(userdata,str_local_path);
            truncate_client = truncate(local_path,newsize);
            if(truncate_client < 0 ){
                DLOG("fail to impl system call stat in client getattr");
                fxn_ret = -errno;
                free(local_path);
                return fxn_ret;
            }
            DLOG("success to impl system call stat in client getattr");
            if(!check_freshness(userdata,path)){
                int upload_server = upload_file_to_server(userdata,path);
                set_tc(userdata,str_local_path);
                if(upload_server < 0){
                    DLOG("fail to impl upload sys call in cli_truncate");
                    free(local_path);
                    fxn_ret = upload_server;
                    return fxn_ret;
                }
            }
            close_client = close(retcode);
//            erase_from_openfile(userdata,str_local_path);
        }
    }else{
        int mode = get_mode(userdata,str_local_path);
        if(mode == O_RDONLY){
            free(local_path);
            return -EMFILE;
        } else{
            DLOG("begin to impl truncate sys call in cli_truncate");
            truncate_client = truncate(local_path,newsize);
            if(truncate_client <0){
                DLOG("fail to impl truncate sys call in cli_truncate");
                free(local_path);
                fxn_ret = truncate_client;
                return fxn_ret;
            }
            if(!check_freshness(userdata,path)){
                int upload_server = upload_file_to_server(userdata,path);
                set_tc(userdata,str_local_path);
                if(upload_server < 0){
                    DLOG("fail to impl upload sys call in cli_truncate");
                    free(local_path);
                    fxn_ret = upload_server;
                    return fxn_ret;
                }
            }
        }
    }
    free(local_path);
    return fxn_ret;
}

// rpc for client truncate
int rpc_cli_truncate(void *userdata, const char *path, off_t newsize){
    // Change the file size to newsize.
//    DLOG("Received truncate call...");

    int ARG_COUNT = 3;

    void **args = (void **) malloc(ARG_COUNT * sizeof(void *));

    int arg_types[ARG_COUNT + 1];

    int pathlen = strlen(path) + 1;
    arg_types[0] =(1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint) pathlen;
    arg_types[1] = (1u << ARG_INPUT) |(ARG_LONG << 16u);
    arg_types[2] = (1u << ARG_OUTPUT) | (ARG_INT << 16u);
    args[0] = (void *) path;
    args[1] = (void *) &newsize;
    int retcode_truncate;
    args[2] = (void *) &retcode_truncate;
    arg_types[3] = 0;

//    DLOG("start truncate call in RPC Client.....");
    int rpc_ret = rpcCall((char *) "truncate", arg_types, args);
    int fxn_ret = 0;
    if (rpc_ret < 0) {
//        DLOG("fail to Received truncate call in RPC Client");
        fxn_ret = -EINVAL;
    } else {
//        DLOG("success to Received truncate call in RPC Client");
        fxn_ret = retcode_truncate;
    }

    if (fxn_ret < 0) {
//        DLOG("Failed to impl system truncate call in RPC server");
    }
    free(args);

    return fxn_ret;
}

int watdfs_cli_fsync(void *userdata, const char *path,
                     struct fuse_file_info *fi) {
    DLOG("receive cli fsync in cli_fsync cache");
    int fxn_ret = 0;
    char *local_path = get_full_cache_path(path, userdata);
    std::string str_local_path(local_path);

    DLOG("begin to upload file to server in cli_fsync");
    int upload_server = upload_file_to_server(userdata,path);
    set_tc(userdata, str_local_path);
    if(upload_server < 0){
        DLOG("fail to upload new file to server in cli_fsync");
        fxn_ret = upload_server;
        return fxn_ret;
    }
    DLOG("success to upload new file in cli_fsync");

    free(local_path);
    return fxn_ret;
}

//rpc for client fsync
int rpc_cli_fsync(void *userdata, const char *path,
              struct fuse_file_info *fi){
    // Force a flush of file data.
//    DLOG("Received fsync call...");

    int ARG_COUNT = 3;

    void **args = (void **) malloc(ARG_COUNT * sizeof(void *));

    int arg_types[ARG_COUNT + 1];

    int pathlen = strlen(path) + 1;
    arg_types[0] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint) pathlen;
    arg_types[1] = (1u << ARG_INPUT) |(1u << ARG_ARRAY) |(ARG_CHAR << 16u)| (uint) sizeof(struct fuse_file_info);
    arg_types[2] = (1u << ARG_OUTPUT) | (ARG_INT << 16u);
    args[0] = (void *) path;
    args[1] = (void *) fi;
    int retcode_fsync;
    args[2] = (void *) &retcode_fsync;
    arg_types[3] = 0;
//    DLOG("start fsync call in RPC Client.....");
    int rpc_ret = rpcCall((char *) "fsync", arg_types, args);
    int fxn_ret = 0;

    if (rpc_ret < 0) {
//        DLOG("fail to Received fsync call in RPC Client");
        fxn_ret = -EINVAL;
    } else {
//        DLOG("success to Received fsync call in RPC Client");
        fxn_ret = retcode_fsync;
    }

    if (fxn_ret < 0) {
//        DLOG("Failed to impl system fsync call in RPC server");
    }
    free(args);

    return fxn_ret;
}

// CHANGE METADATA
int watdfs_cli_utimens(void *userdata, const char *path,
                       const struct timespec ts[2]) {
    DLOG("receive cli_utimens call in cli_utimens");
    char *local_path = get_full_cache_path(path,userdata);
    std::string str_local_path(local_path);

    int fxn_ret = 0;
    int retcode, utimens_client, upload_server,close_client;

    if(!is_open(userdata,path)){
        retcode = download_file_to_client(userdata,path);
        if(retcode < 0){
            fxn_ret = retcode;
            DLOG("fail to impl system call stat in client getattr");
            free(local_path);
            return fxn_ret;
        }else{
            retcode = open(local_path,O_RDWR);
//            add_to_openfile(userdata,str_local_path, O_RDWR, retcode);
//            set_tc(userdata,str_local_path);
            DLOG("begin to impl sys call utimensat in cli_utimens");
            int utimens_client = utimensat(0,local_path,ts,0);
            if(utimens_client < 0){
                DLOG("fali to impl sys call utimensat in cli_utimens");
                fxn_ret = -errno;
                free(local_path);
                return fxn_ret;
            }
            DLOG("success to impl sys call utimensat in cli_utimens");
            if(!check_freshness(userdata,path)){
                upload_server = upload_file_to_server(userdata,path);
                set_tc(userdata, str_local_path);
                if(upload_server < 0){
                    DLOG("fail to impl upload sys call in cli_utimens");
                    free(local_path);
                    fxn_ret = upload_server;
                    return fxn_ret;
                }
            }
            close_client = close(retcode);
//            erase_from_openfile(userdata,str_local_path);
        }
    } else{
        int mode = get_mode(userdata,str_local_path);
        if(mode == O_RDONLY){
            free(local_path);
            return -EMFILE;
        } else{
            DLOG("begin to impl utimens sys call in cli_utimens");
            utimens_client =  utimensat(0,local_path,ts,0);
            if(utimens_client <0){
                DLOG("fail to impl truncate sys call in cli_utimens");
                free(local_path);
                fxn_ret = utimens_client;
                return fxn_ret;
            }
            if(!check_freshness(userdata,path)){
                int upload_server = upload_file_to_server(userdata,path);
                set_tc(userdata, str_local_path);
                if(upload_server < 0){
                    DLOG("fail to impl upload sys call in cli_utimens");
                    free(local_path);
                    fxn_ret = upload_server;
                    return fxn_ret;
                }
            }
        }
    }
    free(local_path);
    return fxn_ret;
}

// rpc for client utimens
int rpc_cli_utimens(void *userdata, const char *path,
                const struct timespec ts[2]){
    // Change file access and modification times.
//    DLOG("Received utimens call...");

    int ARG_COUNT = 3;

    void **args = (void **) malloc(ARG_COUNT * sizeof(void *));

    int arg_types[ARG_COUNT + 1];

    int pathlen = strlen(path) + 1;
    arg_types[0] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | (uint) pathlen;
    arg_types[1] = (1u << ARG_INPUT) |(1u << ARG_ARRAY) |(ARG_CHAR << 16u)| (uint) sizeof(struct timespec) * 2;
    arg_types[2] = (1u << ARG_OUTPUT) | (ARG_INT << 16u);
    args[0] = (void *) path;
    args[1] = (void *) ts;
    int retcode_utimens;
    args[2] = (void *) &retcode_utimens;
    arg_types[3] = 0;
//    DLOG("start utimens call in RPC Client.....");
    int rpc_ret = rpcCall((char *) "utimens", arg_types, args);
    int fxn_ret = 0;

    if (rpc_ret < 0) {
//        DLOG("fail to Received utimens call in RPC Client");
        fxn_ret = -EINVAL;
    } else {
//        DLOG("success to Received utimens call in RPC Client");
        fxn_ret = retcode_utimens;
    }

    if (fxn_ret < 0) {
//        DLOG("Failed to impl system utimens call in RPC server");
    }
    free(args);

    return fxn_ret;
}

// lock rpc
int lock(const char *path, rw_lock_mode_t lock_mode){
    int num_args = 3;
    void **args = (void**) malloc( num_args*sizeof(void*));
    int arg_types[num_args + 1];
    int pathlen = strlen(path) + 1;
    int fxn_ret = 0;

    arg_types[0] = (1 << ARG_INPUT) | (1 << ARG_ARRAY) | (ARG_CHAR << 16) | pathlen;
    args[0] = (void*) path;
    arg_types[1] = (1 << ARG_INPUT) |  (ARG_INT << 16) ; // mode
    args[1] = (void*) &lock_mode;
    arg_types[2] = (1 << ARG_OUTPUT) | (ARG_INT << 16) ;
    int retcode;
    args[2] = (void*) &retcode;

    arg_types[3] = 0;

    int rpc_ret = rpcCall((char *)"lock", arg_types, args);

    if(rpc_ret < 0){
        free(args);
        fxn_ret = -EINVAL;
        return fxn_ret;
    }

    free(args);
    return fxn_ret;
}

// unlock rpc
int unlock(const char *path, rw_lock_mode_t lock_mode){
    int num_args = 3;
    void **args = (void**) malloc( num_args*sizeof(void*));
    int arg_types[num_args + 1];
    int pathlen = strlen(path) + 1;
    int fxn_ret = 0;

    arg_types[0] = (1 << ARG_INPUT) | (1 << ARG_ARRAY) | (ARG_CHAR << 16) | pathlen;
    args[0] = (void*) path;
    arg_types[1] = (1 << ARG_INPUT) |  (ARG_INT << 16) ; // mode
    args[1] = (void*) &lock_mode;
    arg_types[2] = (1 << ARG_OUTPUT) | (ARG_INT << 16) ; // not array, last 2 bytes 0
    int retcode;
    args[2] = (void*) & retcode;
    arg_types[3] = 0;

    int rpc_ret = rpcCall((char *)"unlock", arg_types, args);
    if (rpc_ret < 0) {//rpc_call failed
        free(args);
        fxn_ret = -EINVAL;
        return fxn_ret;
    }

    free(args);
    return fxn_ret;
}