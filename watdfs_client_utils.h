//
// Created by quany on 2/27/2020.
//
#include "watdfs_client.h"
#include "rw_lock.h"



// forward declartion for client side rpc
int rpc_cli_getattr(void *userdata, const char *path, struct stat *statbuf);
int rpc_cli_fgetattr(void *userdata, const char *path, struct stat *statbuf,
                     struct fuse_file_info *fi);
int rpc_cli_mknod(void *userdata, const char *path, mode_t mode, dev_t dev);
int rpc_cli_open(void *userdata, const char *path, struct fuse_file_info *fi);
int rpc_cli_release(void *userdata, const char *path, struct fuse_file_info *fi);
int rpc_cli_read(void *userdata, const char *path, char *buf, size_t size,
                 off_t offset, struct fuse_file_info *fi);
int rpc_cli_write(void *userdata, const char *path, const char *buf,
                  size_t size, off_t offset, struct fuse_file_info *fi);
int rpc_cli_fsync(void *userdata, const char *path,
                  struct fuse_file_info *fi);
int rpc_cli_truncate(void *userdata, const char *path, off_t newsize);
int rpc_cli_utimens(void *userdata, const char *path,
                    const struct timespec ts[2]);
int upload_file_to_server(void *userdata, const char *path);
int download_file_to_client(void *userdata, const char *path);
int lock(const char *path, rw_lock_mode_t lock_mode);
int unlock(const char *path, rw_lock_mode_t lock_mode);


