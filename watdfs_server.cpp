//
// Starter code for CS 454/654
// You SHOULD change this file
//

#include "rpc.h"
#include "debug.h"
INIT_LOG

#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <cstring>
#include <cstdlib>
#include <fuse.h>
#include "rw_lock.h"
#include "rw_lock.cpp"
#include <map>
#include <vector>
#include <utility>
#include <iterator>
#include <iostream>

#define ELOCK -999
#define EUNLOCK -990

struct file_metadata{
    int state;
    rw_lock_t* lock;
};

std::map<std::string, struct file_metadata*> openfileStates;

// Global state server_persist_dir.
char *server_persist_dir = nullptr;

// Important: the server needs to handle multiple concurrent client requests.
// You have to be carefuly in handling global variables, esp. for updating them.
// Hint: use locks before you update any global variable.

// We need to operate on the path relative to the the server_persist_dir.
// This function returns a path that appends the given short path to the
// server_persist_dir. The character array is allocated on the heap, therefore
// it should be freed after use.
// Tip: update this function to return a unique_ptr for automatic memory management.
char *get_full_path(char *short_path) {
    int short_path_len = strlen(short_path);
    int dir_len = strlen(server_persist_dir);
    int full_len = dir_len + short_path_len + 1;

    char *full_path = (char *)malloc(full_len);

    // First fill in the directory.
    strcpy(full_path, server_persist_dir);
    // Then append the path.
    strcat(full_path, short_path);
    DLOG("Full path: %s\n", full_path);

    return full_path;
}

// The server implementation of getattr.
int watdfs_getattr(int *argTypes, void **args) {
    // Get the arguments.
    // The first argument is the path relative to the mountpoint.
    char *short_path = (char *)args[0];
    // The second argument is the stat structure, which should be filled in
    // by this function.
    struct stat *statbuf = (struct stat *)args[1];
    // The third argument is the return code, which should be set be 0 or -errno.
    int *ret = (int *)args[2];

    // Get the local file name, so we call our helper function which appends
    // the server_persist_dir to the given path.
    char *full_path = get_full_path(short_path);

    // Initially we set set the return code to be 0.
    *ret = 0;

    // TODO: Make the stat system call, which is the corresponding system call needed
    // to support getattr. You should use the statbuf as an argument to the stat system call.
    (void)statbuf;


    // Let sys_ret be the return code from the stat system call.
    int sys_ret = 0;
    DLOG("start sys call stat for getattr");
    sys_ret = stat(full_path, statbuf);
    DLOG("Returning code in system call in getattr server: %d", sys_ret);

    if (sys_ret < 0) {
        DLOG(" fail to impl system call in getattr in server");
        *ret = -errno;
    }

    // Clean up the full path, it was allocated on the heap.
    free(full_path);

    DLOG("Returning code in getattr server: %d", *ret);
    // The RPC call succeeded, so return 0.
    return 0;
}

// The server implementation of mknod.
int watdfs_mknod(int *argTypes, void **args) {
    // Get the arguments.

    char *short_path = (char *)args[0];
    mode_t *mode = (mode_t *) args[1];
    dev_t *dev = (dev_t *) args[2];

    int *ret = (int *)args[3];

    char *full_path = get_full_path(short_path);

    // Initially we set set the return code to be 0.
    *ret = 0;

    // TODO: Make the stat system call, which is the corresponding system call needed

    // Let sys_ret be the return code from the stat system call.
    int sys_ret = 0;
    DLOG("start sys call stat for mknod.......");
    sys_ret = mknod(full_path, *mode, *dev);

    if (sys_ret < 0) {
        // If there is an error on the system call, then the return code should
        // be -errno.
        DLOG(" fail to impl system call in mknod in server");
        *ret = -errno;
    }

    // Clean up the full path, it was allocated on the heap.
    free(full_path);

    DLOG("Returning code in mknod server: %d", *ret);
    // The RPC call succeeded, so return 0.
    return 0;
}

// The server implementation of open.
int watdfs_open(int *argTypes, void **args) {
    // Get the arguments.
    // The first argument is the path relative to the mountpoint.
    char *short_path = (char *)args[0];
    // The second argument is the stat structure, which should be filled in
    // by this function.
    struct fuse_file_info *fi = ( struct fuse_file_info *) args[1];
    // The third argument is the return code, which should be set be 0 or -errno.
    int *ret = (int *)args[2];

//    (void )fi;

    // Get the local file name, so we call our helper function which appends
    // the server_persist_dir to the given path.
    char *full_path = get_full_path(short_path);

    // Initially we set set the return code to be 0.
    *ret = 0;

    // Let sys_ret be the return code from the stat system call.
    int sys_ret = 0 ;

    if(openfileStates.find(short_path) == openfileStates.end()){
        openfileStates[short_path] = new struct file_metadata;
        openfileStates[short_path]->lock = new rw_lock_t;
        rw_lock_init(openfileStates[short_path]->lock);
        if (fi->flags & O_ACCMODE == O_RDONLY){
            openfileStates[short_path]->state = 0;
        }else{
            openfileStates[short_path]->state = 1;
        }
    }else{
        int state = openfileStates[short_path]->state;

        if(state == 0 && (fi->flags & O_ACCMODE == O_RDWR)){
            openfileStates[short_path]->state = 1;
        }else{
            if(state == 1 &&  (fi->flags & O_ACCMODE == O_RDWR)){
                return -EACCES;
            }
        }
    }

    DLOG("start sys call stat for open");
     sys_ret= open(full_path, fi->flags);
     fi->fh = sys_ret;
    if (sys_ret < 0) {
        // If there is an error on the system call, then the return code should
        // be -errno.
        DLOG(" fail to impl system call in getattr in server");
        *ret = -errno;
    }

    // Clean up the full path, it was allocated on the heap.
    free(full_path);

    DLOG("Returning code in getattr server: %d", *ret);
    // The RPC call succeeded, so return 0.
    return 0;
}

// The server implemention of release.
int watdfs_release(int *argTypes, void **args){
    char *short_path = (char *)args[0];
    struct fuse_file_info *fi = ( struct fuse_file_info *) args[1];
    int *ret = (int *)args[2];
    char *full_path = get_full_path(short_path);

    *ret = 0;
    (void) fi;

    int sys_ret = 0 ;
    DLOG("start sys call stat for release");
    sys_ret = close(fi->fh);

    if(sys_ret<0){
        DLOG(" fail to impl system call in release in server");
        *ret = -errno;
    }
    free(full_path);

    DLOG("Returning code in getattr server: %d", *ret);
    return 0;
}

// The server implementation of fgetattr.
int watdfs_fgetattr(int *argTypes, void **args) {
    // Get the arguments.
    // The first argument is the path relative to the mountpoint.
    char *short_path = (char *)args[0];

    struct stat *statbuf = (struct stat *)args[1];

    struct fuse_file_info *fi = ( struct fuse_file_info *)args[2];

    int *ret = (int *)args[3];

    char *full_path = get_full_path(short_path);

    *ret = 0;

    (void)statbuf;

    // Let sys_ret be the return code from the stat system call.
    DLOG("Start system call in fgetattr");
    int sys_ret = fstat(fi->fh, statbuf);

    if (sys_ret < 0) {
        DLOG(" fail to impl system call in fgetattr in server");
        *ret = -errno;
    }

    free(full_path);

    DLOG("Returning code in fgetattr server: %d", *ret);
    return 0;
}

// The server implemention of read.
int watdfs_read(int *argTypes, void **args){
    char *short_path = (char *)args[0];
    char *buf = (char *) args[1];
    size_t *size = (size_t *) args[2];
    off_t *offset = (off_t *)args[3];
    struct fuse_file_info *fi = ( struct fuse_file_info *) args[4];
    int *ret = (int *)args[5];

    char *full_path = get_full_path(short_path);

    *ret = 0;

    int sys_ret = 0;
    DLOG("start sys call stat for read");
    sys_ret = pread(fi->fh, buf, *size, *offset);
    DLOG("Returning code in system call in read server: %d", sys_ret);
    if(sys_ret<0){
        DLOG(" fail to impl system call in read in server");
        *ret = -errno;
    }else{
        DLOG(" success to impl system call in read in server");
        *ret = sys_ret;
    }
    free(full_path);
    DLOG("Returning code in read server: %d", *ret);
    return 0;
}

// The server implementation of write.
int watdfs_write(int *argTypes, void **args){
    char *short_path = (char *)args[0];
    char *buf = (char *) args[1];
    size_t *size = (size_t *) args[2];
    off_t *offset = (off_t *)args[3];
    struct fuse_file_info *fi = ( struct fuse_file_info *) args[4];
    int *ret = (int *)args[5];

    char *full_path = get_full_path(short_path);

    *ret = 0;

    int sys_ret = 0;
    DLOG("start sys call stat for write");
    sys_ret = pwrite(fi->fh, buf, *size, *offset);
    DLOG("Returning code in system call in write server: %d", sys_ret);
    if(sys_ret<0){
        DLOG(" fail to impl system call in write in server");
        *ret = -errno;
    } else{
        DLOG(" success to impl system call in write in server");
        *ret = sys_ret;
    }

    free(full_path);
    DLOG("Returning code in write server: %d", *ret);
    return 0;
}

// The server implementation of truncate.
int watdfs_truncate(int *argTypes, void **args){
    char *short_path = (char *)args[0];
    off_t *newsize = (off_t *)args[1];

    int *ret = (int *)args[2];
    char *full_path = get_full_path(short_path);

    *ret = 0;

    int sys_ret = 0;
    DLOG("start sys call  for truncate.......");
    sys_ret = truncate(full_path, *newsize);
    if (sys_ret < 0) {
        DLOG(" fail to impl system call in truncate in server");
        *ret = -errno;
    }

    free(full_path);

    DLOG("Returning code in truncate server: %d", *ret);
    return 0;

}

// the server implementation  of fsync.
int watdfs_fsync(int *argTypes, void **args){
    char *short_path = (char *)args[0];
    struct fuse_file_info *fi = ( struct fuse_file_info *) args[1];
    int *ret = (int *)args[2];
    char *full_path = get_full_path(short_path);
    *ret = 0;

    int sys_ret = 0;
    DLOG("start sys call  for fsync.......");
    sys_ret = fsync(fi->fh);
    if (sys_ret < 0) {
        DLOG(" fail to impl system call in fsync in server through fi->fh");
        *ret = -errno;
    }

    free(full_path);

    DLOG("Returning code in fsync server: %d", *ret);
    return 0;
}

// the server implementation of utimens
int watdfs_utimens(int *argTypes, void **args){
    char *short_path = (char *)args[0];
    struct timespec *ts = ( struct timespec *) args[1];
    int *ret = (int *)args[2];
    char *full_path = get_full_path(short_path);
    *ret = 0;

    int sys_ret = 0;
    DLOG("start sys call  for utimens.......");
    sys_ret = utimensat(0,full_path, ts, 0);
    if (sys_ret < 0) {
        DLOG(" fail to impl system call in utimens in server through fi->fh");
        *ret = -errno;
    }

    free(full_path);

    DLOG("Returning code in fsync server: %d", *ret);
    return 0;

}

int watdfs_lock(int *argTypes, void **args){
    char *short_path = (char*)args[0];
    rw_lock_mode_t * lockmode = (rw_lock_mode_t *) args[1];
    int *ret = (int*)args[2];

    // Get the local file name, so we call our helper function which appends
    // the server_persist_dir to the given path.
    char *full_path = get_full_path(short_path);

    // Initially we set set the return code to be 0.
    *ret = 0;

    // Make the open system call
    int sys_ret = 0;
    sys_ret = rw_lock_lock(openfileStates[short_path]->lock, *lockmode);
    if(sys_ret < 0){
        *ret = ELOCK;
    }
    free(full_path);
    return 0;
}

int watdfs_unlock(int *argTypes, void **args){
    char *short_path = (char*)args[0];
    rw_lock_mode_t * lockmode = (rw_lock_mode_t *) args[1];
    int *ret = (int*)args[2];

    // Initially we set set the return code to be 0.
    *ret = 0;

    // Make the open system call
    int sys_ret = 0;
    sys_ret = rw_lock_unlock(openfileStates[short_path]->lock, *lockmode);
    if(sys_ret < 0){
        *ret = EUNLOCK;
    }

    return 0;
}

// The main function of the server.
int main(int argc, char *argv[]) {
    // argv[1] should contain the directory where you should store data on the
    // server. If it is not present it is an error, that we cannot recover from.
    if (argc != 2) {
        // In general you shouldn't print to stderr or stdout, but it may be
        // helpful here for debugging. Important: Make sure you turn off logging
        // prior to submission!
        // See watdfs_client.c for more details

        DLOG("Usaage:\n", argv[0], " server_persist_dir");
        return -1;
    }
    // Store the directory in a global variable.
    server_persist_dir = argv[1];

    // TODO: Initialize the rpc library by calling `rpcServerInit`.
    // Important: `rpcServerInit` prints the 'export SERVER_ADDRESS' and
    // 'export SERVER_PORT' lines. Make sure you *do not* print anything
    // to *stdout* before calling `rpcServerInit`.
    //DLOG("Initializing server...");
    int rpcSInit = rpcServerInit();
    DLOG("Initializing server...");

    int ret = 0;
    // TODO: If there is an error with `rpcServerInit`, it maybe useful to have
    // debug-printing here, and then you should return.
    if (rpcSInit !=0){
        DLOG("fail to initialize rpc server");
//        std::cerr << "fail to initialize rpc server"<< std::endl;
        return rpcSInit;
    }

    // TODO: Register your functions with the RPC library.
    // Note: The braces are used to limit the scope of `argTypes`, so that you can
    // reuse the variable for multiple registrations. Another way could be to
    // remove the braces and use `argTypes0`, `argTypes1`, etc.
    {
        // There are 3 args for the function (see watdfs_client.c for more
        // detail).
        int argTypes[4];
        // First is the path.
        argTypes[0] =
            (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | 1u;
        // The second argument is the statbuf.
        argTypes[1] =
            (1u << ARG_OUTPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | 1u;
        // The third argument is the retcode.
        argTypes[2] = (1u << ARG_OUTPUT) | (ARG_INT << 16u);
        // Finally we fill in the null terminator.
        argTypes[3] = 0;

        // We need to register the function with the types and the name.
        ret = rpcRegister((char *)"getattr", argTypes, watdfs_getattr);
        if (ret < 0) {
            // It may be useful to have debug-printing here.
            DLOG("fail to register the rpcRegister for getattr");
//            std::cerr << "fail to register the rpcRegister for getattr"<< std::endl;
            return ret;
        }
    }
    //mknod
    {
        int argTypes[5];
        argTypes[0] =
                (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | 1u;
        argTypes[1] = (1u << ARG_INPUT) |  (ARG_INT << 16u);
        argTypes[2] =
                (1u << ARG_INPUT) | (ARG_LONG << 16u);
        argTypes[3] = (1u << ARG_OUTPUT) | (ARG_INT << 16u);
        argTypes[4] = 0;
        ret = rpcRegister((char *)"mknod", argTypes, watdfs_mknod);
        if (ret < 0) {
            DLOG("fail to register the rpcRegister for mknod");
            return ret;
        }
    }

    //open
    {
        int argTypes[4];
        // First is the path.
        argTypes[0] =
                (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | 1u;
        argTypes[1] =
                (1u << ARG_INPUT) |(1u << ARG_OUTPUT) |(1u << ARG_ARRAY) | (ARG_CHAR << 16u) | 1u;
        argTypes[2] = (1u << ARG_OUTPUT) | (ARG_INT << 16u);
        // Finally we fill in the null terminator.
        argTypes[3] = 0;

        // We need to register the function with the types and the name.
        ret = rpcRegister((char *)"open", argTypes, watdfs_open);
        if (ret < 0) {
            // It may be useful to have debug-printing here.
            DLOG("fail to register the rpcRegister for getattr");
            return ret;
        }
    }

    //release
    {
        int argTypes[4];
        argTypes[0] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | 1u;
        argTypes[1] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | 1u;
        argTypes[2] = (1u << ARG_OUTPUT) | (ARG_INT << 16u);
        argTypes[3] = 0;

        DLOG("begin to register the rpcRegister for release");
        ret = rpcRegister((char *) "release", argTypes, watdfs_release);
        if(ret<0){
            DLOG("fail to register the rpcRegister for release");
            return ret;
        }
    }

      //fgetattr
    {
        int argTypes[5];
        argTypes[0] =
                (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | 1u;
        argTypes[1] = (1u << ARG_OUTPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u)|1u;
        argTypes[2] =
                (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | 1u;
        argTypes[3] = (1u << ARG_OUTPUT) | (ARG_INT << 16u);
        argTypes[4] = 0;
        ret = rpcRegister((char *)"fgetattr", argTypes, watdfs_fgetattr);
        if (ret < 0) {
            DLOG("fail to register the rpcRegister for fgetattr");
            return ret;
        }
    }

    //read
    {
        int argTypes[7];
        argTypes[0] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | 1u;
        argTypes[1] = (1u << ARG_OUTPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u)|1u;
        argTypes[2] = (1u << ARG_INPUT) | (ARG_LONG << 16u);
        argTypes[3] = (1u << ARG_INPUT) | (ARG_LONG << 16u);
        argTypes[4] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | 1u;
        argTypes[5] = (1u << ARG_OUTPUT) | (ARG_INT << 16u);
        argTypes[6] = 0;
        ret = rpcRegister((char *) "read", argTypes, watdfs_read);
    }

    //write
    {
        int argTypes[7];
        argTypes[0] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | 1u;
        argTypes[1] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u)|1u;
        argTypes[2] = (1u << ARG_INPUT) | (ARG_LONG << 16u);
        argTypes[3] = (1u << ARG_INPUT) | (ARG_LONG << 16u);
        argTypes[4] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | 1u;
        argTypes[5] = (1u << ARG_OUTPUT) | (ARG_INT << 16u);
        argTypes[6] = 0;
        ret = rpcRegister((char *) "write", argTypes, watdfs_write);
    }

    // truncate
    {
        int argTypes[4];
        argTypes[0] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | 1u;
        argTypes[1] = (1u << ARG_INPUT) | (ARG_LONG << 16u) ;
        argTypes[2] = (1u << ARG_OUTPUT) | (ARG_INT << 16u);
        argTypes[3] = 0;
        ret = rpcRegister((char *) "truncate", argTypes, watdfs_truncate);
    }

    // fsync
    {
        int argTypes[4];
        argTypes[0] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | 1u;
        argTypes[1] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) |(ARG_CHAR << 16u) | 1u;
        argTypes[2] = (1u << ARG_OUTPUT) | (ARG_INT << 16u);
        argTypes[3] = 0;
        ret = rpcRegister((char *) "fsync", argTypes, watdfs_fsync);
    }

    //utimens
    {
        int argTypes[4];
        argTypes[0] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) | (ARG_CHAR << 16u) | 1u;
        argTypes[1] = (1u << ARG_INPUT) | (1u << ARG_ARRAY) |(ARG_CHAR << 16u) | 1u;
        argTypes[2] = (1u << ARG_OUTPUT) | (ARG_INT << 16u);
        argTypes[3] = 0;
        ret = rpcRegister((char *) "utimens", argTypes,watdfs_utimens);
    }

    { //lock
        int num_args = 3;
        int argTypes[num_args+1];
        argTypes[0] = (1 << ARG_INPUT) | (1 << ARG_ARRAY) | (ARG_CHAR << 16) | 1;
        argTypes[1] = (1 << ARG_INPUT) | (ARG_INT << 16) ;
        argTypes[2] = (1 << ARG_OUTPUT) | (ARG_INT << 16) ;
        argTypes[3] = 0;

        ret = rpcRegister((char*)"lock", argTypes, watdfs_lock);
        if (ret < 0) {
            return ret;
        }
    }

    { //unlock
        int num_args = 3;
        int argTypes[num_args+1];
        argTypes[0] = (1 << ARG_INPUT) | (1 << ARG_ARRAY) | (ARG_CHAR << 16) | 1;
        argTypes[1] = (1 << ARG_INPUT) | (ARG_INT << 16) ;
        argTypes[2] = (1 << ARG_OUTPUT) | (ARG_INT << 16) ;
        argTypes[3] = 0;

        ret = rpcRegister((char*)"unlock", argTypes, watdfs_unlock);
        if (ret < 0) {
            return ret;
        }
    }

    // TODO: Hand over control to the RPC library by calling `rpcExecute`.
    int rpcSExec = rpcExecute();

    if (rpcSExec != 0){
        DLOG("Failed to execute rpcExecute server");
        return rpcSExec;
    }else{
        DLOG("Success to execute rpcExecute server");
    }

    std::map<std::string, struct file_metadata *> :: iterator itr;
    for(itr = openfileStates.begin(); itr != openfileStates.end(); itr++){
        rw_lock_t* lock = itr->second->lock;
        rw_lock_destroy(lock);
        delete itr->second;
    }

    return ret;
}
