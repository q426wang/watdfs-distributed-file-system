# Distributed File System

In this project, I designed a distributed file system to support clients create, open, read, write and close files on the remote machine. It can execute in Linux environment. This project followed the typical client-server architecture.

I implemented two standard distributed file system model.  One is remote-access model. Another is download and upload model. 

## System Manual

You can find the system manual in [link to manual](https://git.uwaterloo.ca/q426wang/watdfs-distributed-file-system/-/blob/master/manual.pdf)

## Execution
1. to compile your code:

```make clean all```

2. to run your server(in terminal 1 on eg., ubuntu1804-008):

```mkdir -p /tmp/$user/server```

```./wardfe_server /temp/$user/server```

3. to run your client(in terminal 2 on eg., ubuntu1804-004):

```export SERVER_ADDRESS=UBUNTU1804-008```

```export SERVER_PORT=12345```

```mkdir -p /tmp/$user/cache /tmp/$user/mount```

```./wardfs_client -s -f -o direct_io /tmp/$user/cache /tmp/$user/mount```


4. to test you client with bash commands(in terminal 3 on the same server as watdfs_client, e.g, ubuntu1804-004 above):

```touch /tmp/$user/mount/myfile.txt```

```echo "cs654 is fun" > /tmp/$user/mount/myfile.txt```

```cat /tmp/$user/mount/myfile.txt```

```stat /tmp/$user/mount/myfile.txt```


